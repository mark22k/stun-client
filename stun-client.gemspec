
require_relative 'lib/stun-client/module'

# rubocop:disable Layout/FirstArrayElementIndentation
# rubocop:disable Layout/FirstHashElementIndentation
Gem::Specification.new do |spec|
  spec.name = 'stun-client'
  spec.version = StunClient::VERSION
  spec.summary = 'Library to act with STUN servers.'
  spec.description = <<DESCRIPTION.strip.tr("\n", ' ')
  Library to act with STUN servers. It implements RFC3489 ("classic" STUN). It
  has no authentication methods against STUN servers. It includes functions to
  request STUN servers. Furthermore there is the possibility to detect the NAT
  type.
DESCRIPTION
  spec.authors = ['Marek Küthe']
  spec.email = 'm.k@mk16.de'

  spec.files = [
                 'lib/stun-client/generic/client.rb',
                 'lib/stun-client/generic/errors.rb',
                 'lib/stun-client/generic/utilities.rb',
                 'lib/stun-client/rfc3489/client.rb',
                 'lib/stun-client/rfc3489/message.rb',
                 'lib/stun-client/rfc3489/discovery/nat.rb',
                 'lib/stun-client/rfc3489/discovery/lifetime.rb',
                 'lib/stun-client/module.rb'
               ]
  spec.extra_rdoc_files = [
                            'LICENSE',
                            'README.md'
                          ]

  spec.homepage = 'https://codeberg.org/mark22k/stun-client'
  spec.license = 'GPL-3.0-or-later'

  spec.metadata = {
                    'source_code_uri' => 'https://codeberg.org/mark22k/stun-client',
                    'bug_tracker_uri' => 'https://codeberg.org/mark22k/stun-client/issues',
                    'rubygems_mfa_required' => 'true'
                  }

  spec.required_ruby_version = '>= 3.1.1'
  spec.add_runtime_dependency 'bindata', '~> 2.4', '>= 2.4.15'
end
# rubocop:enable Layout/FirstArrayElementIndentation
# rubocop:enable Layout/FirstHashElementIndentation
