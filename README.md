# stun-client

## Modules

- RFC3489
  - Supported:
    - Bindung request
    - Bindung response
  - Not supported:
    - Integrity with Shared Secret
