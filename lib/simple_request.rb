require_relative 'stun-client/rfc3489/discovery/lifetime'

socket = UDPSocket.new
socket.do_not_reverse_lookup = true

client = StunClient::RFC3489Client.new(socket:)
resp = client.query

pp resp
