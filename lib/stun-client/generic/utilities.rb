module StunClient
  # Contains useful functions used by different parts of the STUN client
  # implementation. These functions can act independently of the context.
  class Utilities
    require 'securerandom'

    # Generates a random transaction ID. It contains the characters 0-9 and a-f.
    #
    # @param bits [Integer] The number of bits the transaction ID should be long.
    def self.generate_txid bits
      bytes = bits / 8
      raise 'A TxID with bits not divisible by eight is not possible.' if bits % 8 != 0

      bytes /= 2  # as .hex doubles the length
      return SecureRandom.hex(bytes)
    end

    # Attempts to sanitize a string before it is processed further. This
    # tries to escape some security holes. For sanitizing the function
    # `dump` is used.
    #
    # @param str [String] String which is to be sanitized.
    # @return Sanitized string
    def self.sanitize_string str
      return str.dump[1...-1]
    end
  end
end
