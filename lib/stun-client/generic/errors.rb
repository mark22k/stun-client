module StunClient
  # Generic error
  class StunError < RuntimeError; end

  # The server has responded to a binding request with a binding error response.
  # Further information can be found in the error message.
  class BindungErrorResponse < StunError; end

  # The STUN client could not interpret the response from the server. This may
  # mean, for example, that there was a transmission error or that the server is
  # using an unknown format.
  class UnableToInterpretResponse < StunError; end

  # The server's response does not comply with the norm.
  class InvalidResponse < StunError; end

  # The server replied with a wrong transaction ID. It may be that a response
  # was received which was for someone else.
  class InvalidTransactionId < InvalidResponse; end

  # The server does not respond. It may be that it is not a stun server or
  # that UDP is blocked.
  class TimeoutError < StunError; end
end
