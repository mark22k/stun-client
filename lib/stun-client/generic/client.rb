module StunClient
  # Generic STUN client. This contains functions which are used by all STUN
  # clients independent of the RFC.
  class GenericClient
    require 'ipaddr'
    require_relative 'errors'

    # Creates a new generic client.
    #
    # @param parameters[Hash]
    #   :host (optional) The STUN server. By default `stun.1and1.de` (German provider).
    #   :port (optional) The port of the STUN server. By default 3478.
    #   :socket A UDPSocket through which the requests are sent to the server.
    def initialize parameters
      raise ArgumentError, 'Missing socket' if ! parameters.has_key? :socket
      raise ArgumentError, 'Socket must be a UDPSocket' if ! parameters[:socket].is_a? UDPSocket

      if parameters.has_key?(:host) && parameters[:host]
        raise ArgumentError, 'Host must be a string' if ! parameters[:host].is_a? String
      else
        parameters[:host] = 'stun.1und1.de'
      end

      if parameters.has_key?(:port) && parameters[:port]
        raise ArgumentError, 'Port must be a integer' if ! parameters[:port].is_a? Integer
      else
        parameters[:port] = 3478
      end

      @socket = parameters[:socket]
      @host = parameters[:host]
      @port = parameters[:port]
    end

    # Parses an IP port pair and returns a hash with the information.
    #
    # It throws the exception `UnableToInterpretResponse` if the IP version is
    # unknown. Known are IPv4 and IPv6.
    #
    # The `UnableToInterpretResponse` is thrown if the IP address is not encoded
    # correctly.
    #
    # @param value [Hash] A hash containing the values of a MappedAddress attribute.
    # @param prefix [String] Prefix which is prefixed to each key of returned.
    # @return [Hash]
    def parse_mapped_address value, prefix = nil
      res = {}
      family_sym = "#{prefix}family".to_sym
      host_sym = "#{prefix}host".to_sym
      port_sym = "#{prefix}port".to_sym

      case value[:family]
      when 1
        res[family_sym] = :IPv4
      when 2
        res[family_sym] = :IPv6
      else
        raise UnableToInterpretResponse, "Unknown IP family. Family is #{value[family_sym].inspect}"
      end

      begin
        res[host_sym] = IPAddr.ntop value[:address]
      rescue IPAddr::AddressFamilyError
        raise UnableToInterpretResponse, 'Failed to parse ip address'
      end

      res[port_sym] = value[:port]

      return res
    end

    # The function validates the Change IP and Change Port parameters and
    # returns those. If no parameters are specified, the default value false
    # is used.
    #
    # @param parameters [Hash] Hash containing `:change_ip` and `:change_port`.
    #                          These values should be either `true`, `false`
    #                          or `nil`.
    # @return [Array] Returns an array with two integers. If Change Port is
    #                 true, the first integer is 1, otherwise 0. The same is
    #                 applies for Change IP and the second integer.
    def validate_and_get_change parameters
      change_port = 0
      change_ip = 0

      if parameters.has_key?(:change_port) && parameters[:change_port]
        if ! (parameters[:change_port].is_a?(TrueClass) || parameters[:change_port].is_a?(FalseClass))
          raise ArgumentError, 'Change port must be a boolean'
        end

        change_port = ( parameters[:change_port] ? 1 : 0 )
      end

      if parameters.has_key?(:change_ip) && parameters[:change_ip]
        if ! (parameters[:change_ip].is_a?(TrueClass) || parameters[:change_ip].is_a?(FalseClass))
          raise ArgumentError, 'Change IP must be a boolean'
        end

        change_ip = ( parameters[:change_ip] ? 1 : 0 )
      end

      return change_port, change_ip
    end

    # Sends a request in binary form to a server.
    #
    # `ArgumentError` is thrown if one of the arguments is not valid.
    # `TimeoutError` is thrown if the server does not respond.
    #
    # @param parameters [Hash] Contains `:attempts` for the number of attempts
    #                          to send the message to the server.
    # @param request [String] A binary string to be sent to the server.
    # @param new_interval [Proc] A lambda function. This is called after each
    #                            failed attempt to get a response from the
    #                            server. As argument it gets the current interval
    #                            in which the message is tried to be sent to the
    #                            server. As return value a new interval is expected,
    #                            which should be waited for. The result is rounded to
    #                            three decimal places outside the function. The interval
    #                            is specified in seconds.
    # @param attempts_default [Integer] The default number of attempts to send the
    #                                   message. Can be overridden by `parameters`.
    # @param interval_default [Integer] The default initial interval. Can be overwritten
    #                                   by `parameters`.
    # @return [Array] Response from the server. See UDPSocket#recvfrom_nonblock
    def send_binary parameters, request, new_interval, attempts_default, interval_default
      attempts = attempts_default
      if parameters.has_key?(:attempts) && parameters[:attempts]
        attempts = parameters[:attempts]
        raise ArgumentError, 'Attepts must be a integer' if ! attempts.is_a? Integer
      end

      interval = interval_default
      if parameters.has_key?(:interval) && parameters[:interval]
        interval = parameters[:interval]
        raise ArgumentError, 'Interval must be a integer' if ! (interval.is_a?(Integer) || interval.is_a?(Float))
      end
      interval = interval.to_f

      response = try_sending request, attempts, interval, new_interval

      raise TimeoutError, 'Server failed to answer' if ! response

      return response
    end

    # Attempts to send a message `request` `attempts` times to the server.
    # The meaning of the intervals can be taken from the `send_binary` function.
    #
    # @param request [String]
    # @param attempts [Integer]
    # @param interval [Integer]
    # @param new_interval [Proc]
    # @return Returns either a response (see UDPSocket#recvfrom_nonblock) or
    #         false if the server did not respond.
    def try_sending request, attempts, interval, new_interval
      attempts.times do |index|
        @socket.send(request, 0, @host, @port)

        # Waiting for answer. No remote server should be able
        # to respond **immediately**.
        sleep interval

        resp = receive_message
        return resp if resp

        if index != (attempts - 1)
          interval = new_interval.call(interval).round(3)
        end
      end

      return false
    end

    # Attempts to receive a response from the server. If there is no
    # response, `false` is returned.
    def receive_message
      begin
        @socket.recvfrom_nonblock(1024)
      rescue IO::WaitReadable
        return false
      end
    end
  end
end
