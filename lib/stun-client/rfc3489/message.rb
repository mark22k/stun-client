module StunClient
  # Contains BinData records which can be used to represent a STUN message according to RFC3489.
  module RFC3489Message
    require 'bindata'

    # Contains a coded IP address, its version number and a port. The IP address
    # can be further decoded later with `IPAddr.ntop`.
    class MappedAddress < BinData::Record
      endian :big

      uint16 :family
      uint16 :port
      string :address, length: -> { ( family == 1 ? 4 : ( family == 2 ? 16 : (attribute_length - 4))) }
    end

    # Represents the content of the 'CHANGE-REQUEST' attribute. Only two flags
    # can be set. The rest consists of zeros.
    class ChangeRequest < BinData::Record
      endian :big

      # Skip 29 bits
      bit29

      # flags
      bit1 :change_ip
      bit1 :change_port

      # skip one bit
      bit1
    end

    # Represents a simple string. This type can be used to describe attributes
    # that contain only characters.
    class SimpleString < BinData::Record
      endian :big

      string :msg, length: :attribute_length
    end

    # Represents the content of the attribute 'ERROR-CODE'. This contains an
    # error code like HTTP and an error message.
    class ErrorCode < BinData::Record
      # rubocop:disable Layout/FirstHashElementIndentation
      # rubocop:disable Layout/HashAlignment

      # Hash, which contains the error messages for the error class and the error number.
      ERROR_MESSAGES = {
        4 => {
           0 => 'Bad request',
           1 => 'Unauthorized',
          20 => 'Unknown Attribute',
          30 => 'Stale Credentials',
          31 => 'Integrity Check Failure',
          32 => 'Missing Username',
          33 => 'Use TLS'
        }.freeze,
        5 => {
           0 => 'Server Error'
        }.freeze,
        6 => {
           0 => 'Global Failure:'
        }.freeze
      }.freeze
      # rubocop:enable Layout/FirstHashElementIndentation
      # rubocop:enable Layout/HashAlignment

      # Returns an error message for an error code.
      #
      # @param error_class [Integer] Number containing the error class.
      #                              Currently 4, 5 and 6 are supported.
      # @param error_number [Integer] Number, which contains the error number.
      #                               This further classifies the error.
      # @return [String] Returns the error message. If the error number is unknown,
      #                  `'Unknown error number'` or `'Unknown error class'`
      #                  accordingly.
      def get_error_message error_class, error_number
        if ERROR_MESSAGES.has_key? error_class
          if ERROR_MESSAGES[error_class].has_key? error_number
            return ERROR_MESSAGES[error_class][error_number]
          end

          return 'Unknown error number'
        end
        return 'Unknown error class'
      end

      endian :big

      bit21
      bit3 :error_class
      bit8 :error_number
      string :error_message, length: -> { attribute_length - 4 }
    end

    # Represents the content of the attribute 'UNKNOWN-ATTRIBUTES'. This
    # contains a list with the codes of the attributes that the server did
    # not understand.
    class UnknownAttribute < BinData::Record
      endian :big

      array :unknown_attributes, read_until: -> { index * 4 >= attribute_length }, type: :uint16
    end

    # Represents an attribute of a STUN message according to RFC3489
    class Attribute < BinData::Record
      endian :big

      uint16 :attribute_type
      uint16 :attribute_length
      choice :attribute_value, selection: -> { attribute_type } do
        MappedAddress     0x0001  # mapped address
        MappedAddress     0x0002  # response address
        ChangeRequest     0x0003  # change request
        MappedAddress     0x0004  # source address
        MappedAddress     0x0005  # changed address
        SimpleString      0x0006  # username
        SimpleString      0x0007  # password
        SimpleString      0x0008  # hmac
        ErrorCode         0x0009  # error code
        UnknownAttribute  0x000a  # unknown attribute
        MappedAddress     0x000b  # reflected from

        SimpleString :default
      end
    end

    # Represents a complete STUN message according to RFC3489 using BinData.
    # Only the responses are extracted, but none are evaluated.
    #
    # If necessary, the name of the message or an attribute can be queried
    # using a function.
    class StunMessage < BinData::Record
      # Hash with the hexadecimal type numbers and their names as symbols.
      MESSAGE_TYPES = {
        0x0001 => :bindung_request,
        0x0101 => :binding_response,
        0x0111 => :bindung_error_response,
        0x0002 => :shared_secret_request,
        0x0102 => :shared_secret_response,
        0x0112 => :shared_secret_error_response
      }.freeze

      # Hash with the hexadecimal attribute numbers and their names as symbols.
      ATTRIBUTE_TYPES = {
        # RFC3489
        0x0001 => :mapped_address,
        0x0002 => :response_address,
        0x0003 => :change_request,
        0x0004 => :source_address,
        0x0005 => :changed_address,
        0x0006 => :username,
        0x0007 => :password,
        0x0008 => :message_integrity,
        0x0009 => :error_code,
        0x000a => :unknown_attributes,
        0x000b => :reflected_from
      }.freeze

      endian :big

      uint16 :message_type
      uint16 :message_length
      string :transaction_id, length: 16

      array :attributes, read_until: :eof, type: Attribute
    end
  end
end
