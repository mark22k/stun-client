
module StunClient
  # Helper, which should determine the NAT type.
  class RFC3489NATDiscovery
    require 'socket'
    require_relative '../../generic/errors'
    require_relative '../client'

    # Returns an array with the IP addresses of the local device.
    # @return [Array]
    # rubocop:disable Naming/AccessorMethodName
    def self.get_local_addresses
      return Socket.ip_address_list.map(&:ip_address)
    end
    # rubocop:enable Naming/AccessorMethodName

    # Creates a new object for NAT discovery.
    #
    # @param host [String] The host of the STUN server to be used for NAT
    #                      discovery. By default, the default server of the
    #                      STUN client is used.
    # @param port [Integer] The port of the STUN server.
    # @param protocol [Symbol] Defines whether the STUN server should be
    #                          reached via IPv4 or IPv6. Accordingly like
    #                          a NATv4 or NATv6 detected. Possible values
    #                          are `:IPv4` or `:IPv6`.
    def initialize host = nil, port = nil, protocol = :IPv4
      raise ArgumentError, 'Host must be a string' if host && (! host.is_a? String)
      raise ArgumentError, 'Port must be a integer' if port && (! port.is_a? Integer)

      case protocol
      when :IPv6
        inet = Socket::AF_INET6
        addr = '::'
      when :IPv4
        inet = Socket::AF_INET
        addr = '0.0.0.0'
      else
        raise ArgumentError, 'Unknown protocol'
      end

      @host = host
      @port = port

      @socket = UDPSocket.new inet
      @socket.do_not_reverse_lookup = true
      @socket.bind addr, 0
      @source_port = @socket.addr[1]
    end

    # Checks the NAT Type and returns an array with a corresponding description.
    # In the worst case, it can take up to 38 seconds for the function to finish.
    #
    # *Important*: Keep in mind that RFC3489 is outdated and that measurements
    # are not always accurate.
    #
    # @return [Array]
    #   *:preserves_ports* The local ports are mapped to the outside. Example: If
    #   a socket is created locally with port 4000, this is
    #   also the source port when a server is requested.
    #   *:random_port* The opposite of `:preserves_ports`.
    #
    #   *:nat* The device is located behind a NAT.
    #   *:no_nat* The device is not located behind a NAT.
    #
    #   *:open_internet* The device is openly located on the Internet. No filtering
    #   is performed.
    #   *:symmetric_udp_firewall* The device is located behind a firewall that
    #   filters by source port and source address.
    #
    #   *:full_cone_nat* The device is located behind a Full Cone NAT. The NAT does
    #   not filter the source port or source address.
    #   *:restricted_nat* The device is behind a restrictive NAT. The NAT filters
    #   only by source address, but not by source port.
    #   *:port_restricted_nat* The device is located behind a port-restricted NAT.
    #   The NAT filters by source port and source address.
    #
    #   *:udp_blocked* The STUN server could not be reached. Therefore, the
    #   measurement could not be executed.
    #   *:udp_sometimes_blocked* This case should never leak. It means that an
    #   inconsistent NAT/firewall is used. One of the
    #   requests to the STUN server was blocked.
    #   Therefore, the measurement could not be completed.
    def detect_nat
      # rubocop:disable Style/ConditionalAssignment
      flags = []

      test1v1 = do_query @host, @port

      if test1v1
        if test1v1[:port] == @source_port
          flags << :preserves_ports
        else
          flags << :random_port
        end

        if RFC3489NATDiscovery.get_local_addresses.include?(test1v1[:host]) &&
           test1v1[:port] == @source_port

          flags << :no_nat

          test2 = do_query @host, @port, change_ip: true, change_port: true
          if test2
            flags << :open_internet
          else
            flags << :symmetric_udp_firewall
          end
        else
          flags << :nat

          test2 = do_query @host, @port, change_ip: true, change_port: true
          if test2
            flags << :full_cone_nat
          else
            test1v2 = do_query test1v1[:changed_host], test1v1[:changed_port].to_i
            if test1v2
              if test1v1[:host] == test1v2[:host] &&
                 test1v1[:port] == test1v2[:port]
                test3 = do_query @host, @port, change_port: true
                if test3
                  flags << :restricted_nat
                else
                  flags << :port_restricted_nat
                end
              else
                flags << :symmetric_nat
              end
            else
              flags << :udp_sometimes_blocked
            end
          end
        end

      else
        flags << :udp_blocked
      end

      # flags.sort!
      return flags

      # rubocop:enable Style/ConditionalAssignment
    end

    private

    # Makes a request to a STUN server.
    #
    # @param host [String]
    # @param port [Integer]
    # @param parameters [Hash]
    # @return [Hash, FalseClass]
    def do_query host, port, parameters = {}
      begin
        client = StunClient::RFC3489Client.new(host:, port:, socket: @socket)
        resp = client.query parameters

        raise StunClient::InvalidResponse, 'Response does not contain a mapped address.' if ! resp.has_key? :family
        if ! resp.has_key? :changed_family
          raise StunClient::InvalidResponse, 'Response does not contain a changed address.'
        end
        raise StunClient::UnableToInterpretResponse, 'IP version mismatch.' if resp[:family] != resp[:changed_family]

        return resp
      rescue StunClient::TimeoutError
        return false
      end
    end

  end
end
