
module StunClient
  # Helper to determine the Binding Lifetime of a NAT.
  #
  # *Note*: This helper has not been tested yet. The reason is that I have
  # not found a STUN server that supports the Response Address Attribute.
  class RFC3489LifetimeDiscovery
    require 'socket'
    require_relative '../../generic/errors'
    require_relative '../client'

    # Creates a new object for NAT lifetime discovery.
    #
    # @param host [String] The host of the STUN server to be used for NAT
    #                      discovery. By default, the default server of the
    #                      STUN client is used.
    # @param port [Integer] The port of the STUN server.
    # @param protocol [Symbol] Defines whether the STUN server should be
    #                          reached via IPv4 or IPv6. Accordingly like
    #                          a NATv4 or NATv6 detected. Possible values
    #                          are `:IPv4` or `:IPv6`.
    def initialize host = nil, port = nil, protocol = :IPv4
      raise ArgumentError, 'Host must be a string' if host && (! host.is_a? String)
      raise ArgumentError, 'Port must be a integer' if port && (! host.is_a? Integer)

      case protocol
      when :IPv6
        @inet = Socket::AF_INET6
        @addr = '::'
      when :IPv4
        @inet = Socket::AF_INET
        @addr = '0.0.0.0'
      else
        raise ArgumentError, 'Unknown protocol'
      end

      @host = host
      @port = port
    end

    # when max is the lifetime or the lifetime is higher,
    # it returns max
    # when the is lower then min, it returns min
    # if min and max are equal, it returns [min/max]

    # Measures the Binding Lifetime of a NAT.
    #
    # @param max [Integer] Specifies the maximum lifetime after which the
    #                      measurement is to be aborted.
    # @param min [Integer] Specifies the minimum lifetime. If the lifetime
    #                      is less than this, the program is aborted. It
    #                      is recommended to leave this at zero.
    # @return [Integer] Specifies the NAT Binding Lifetime. If it is greater
    #                   than the allowed maximum, the maximum itself is
    #                   returned. If it is less than the allowed minimum, the
    #                   minimum itself is returned.
    def detect_binding_lifetime max = 1200, min = 0
      raise 'STUN server does not support response address.' if ! alive_in_time?(0)

      while min < max
        mid = (min + max) / 2

        if alive_in_time? mid
          min = mid + 1
        else
          max = mid - 1
        end
      end

      return max
    end

    # Checks if the NAT Binding Lifetime is less than the specified time.
    #
    # For this, a STUN server is contacted to determine its own host and port.
    # Afterwards it is waited accordingly. Then the STUN server is contacted
    # again from another socket and instructed to send a STUN binding response
    # to the previous socket. If the response arrives, the binding lifetime has
    # not been exceeded. If no response is received, the binding lifetime is
    # exceeded.
    #
    # @param time [Integer]
    # @return [TrueClass, FalseClass] Returns false if the binding lifetime has
    #                                 been exceeded, otherwise true.
    def alive_in_time? time
      raise 'The waiting time must be an integer.' if ! time.is_a? Integer

      socket_a = create_socket
      resp = do_query socket_a
      raise TimeoutError, 'UDP blocked.' if ! resp

      host = resp[:host]
      port = resp[:port].to_i

      # rubocop:disable Style/NumericLiterals
      raise 'The server has returned an invalid port.' if port.zero? || port > 65535
      # rubocop:enable Style/NumericLiterals

      sleep time

      socket_b = create_socket
      resp = do_query socket_b, response_ip: host, response_port: port

      return ( resp ? true : false )
    end

    private

    # Makes a request to a STUN server.
    #
    # @param socket [UDPSocket]
    # @param parameters [Hash]
    # @return [Hash, FalseClass]
    def do_query socket, parameters = {}
      begin
        client = StunClient::RFC3489Client.new(host: @host, port: @port, socket:)
        resp = client.query parameters

        raise StunClient::InvalidResponse, 'Response does not contain a mapped address.' if ! resp.has_key? :family
        raise StunClient::UnableToInterpretResponse, 'IP version mismatch.' if resp[:family] != resp[:changed_family]

        return resp
      rescue StunClient::TimeoutError
        return false
      end
    end

    # Creates a new UDPSocket
    #
    # @return [UDPSocket]
    def create_socket
      socket = UDPSocket.new @inet
      socket.do_not_reverse_lookup = true
      socket.bind @addr, 0

      return socket
    end
  end
end
