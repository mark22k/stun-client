
module StunClient
  require_relative '../generic/client'
  require_relative '../generic/utilities'
  require_relative '../generic/errors'

  # A STUN client which can send and receive RFC3489 messages. It does not
  # support shared secret messages.
  #
  # Personal note: I would have liked to implement this, but I have not found
  # a STUN server that supports it.
  class RFC3489Client < StunClient::GenericClient
    require_relative 'message'
    require 'ipaddr'

    # The parameters for creating this class can be taken
    # from StunClient::GenericClient.

    # Make a STUN Binding Request.
    #
    # @param parameters [Hash]
    #   If the change IP or change port flag is to be set, `:change_ip` or
    #   `:change_port` can be set to true.
    #
    #   If the default value (RFC taken) for retransmission should be changed,
    #   `:attempts` can be set for the number of attempts to transmit the request.
    #   Default value is 9. To change the initial interval (default value 100ms =
    #   0.1) `:interval` can be set. This doubles according to RFC for each new
    #   attempt until to 1.6 seconds is reached.
    # @return [Hash]
    def query parameters = {}
      txid = StunClient::Utilities.generate_txid(128)

      change_port, change_ip = validate_and_get_change parameters

      request = RFC3489Message::StunMessage.new

      request[:message_type] = 0x0001  # binding request
      request[:transaction_id] = txid
      request[:message_length] = request.to_binary_s.length - 20

      if change_port == 1 || change_ip == 1
        cr = RFC3489Message::ChangeRequest.new
        cr[:change_ip] = change_ip
        cr[:change_port] = change_port

        attr = RFC3489Message::Attribute.new
        attr[:attribute_type] = 0x0003
        attr[:attribute_length] = cr.to_binary_s.length
        attr[:attribute_value] = cr

        request[:message_length] += attr.to_binary_s.length
        request[:attributes] << attr
      end

      if parameters.has_key?(:response_port) && parameters[:response_port] &&
         parameters.has_key?(:response_ip) && parameters[:response_ip]
        raise 'Response Port must be a Integer' if ! parameters[:response_port].is_a? Integer
        raise 'Response IP must be a String' if ! parameters[:response_ip].is_a? String

        family = parameters[:response_ip].include?(':') ? 2 : 1

        begin
          ip = IPAddr.new(parameters[:response_ip]).hton
        rescue IPAddr::InvalidAddressError
          raise ArgumentError, 'Invalid IP'
        end

        ra = RFC3489Message::MappedAddress.new
        ra[:family] = family
        ra[:address] = ip
        ra[:port] = parameters[:response_port]

        attr = RFC3489Message::Attribute.new
        attr[:attribute_type] = 0x0002
        attr[:attribute_length] = ra.to_binary_s.length
        attr[:attribute_value] = ra

        request[:message_length] += attr.to_binary_s.length
        request[:attributes] << attr
      end

      new_interval = ->(old_interval) { (old_interval < 1.6 ? old_interval * 2 : old_interval ) }
      response = send_binary parameters, request.to_binary_s, new_interval, 9, 0.1

      msg = RFC3489Message::StunMessage.read response[0]
      res = {}
      res[:response] = msg

      if msg[:transaction_id] != txid
        expected = StunClient::Utilities.sanitize_string txid
        actual = StunClient::Utilities.sanitize_string msg[:transaction_id]
        # rubocop:disable Layout/TrailingWhitespace
        raise InvalidTransactionId, 
              "Server responsed with invalid transaction id. Expected: #{expected}, Actual: #{actual}"
        # rubocop:enable Layout/TrailingWhitespace
      end

      case RFC3489Message::StunMessage::MESSAGE_TYPES[msg[:message_type]]
      when :binding_response
        res.merge! parse_attributes(msg[:attributes])
      when :bindung_error_response
        res.merge! parse_attributes(msg[:attributes])
        msg = 'Server returns an unknown error'
        if res[:error]
          if res[:error][:unknown_attributes]
            msg += ": Unknown attributes: #{res[:error][:unknown_attributes]}"
          end
          if res[:error][:class]
            error_message = StunClient::Utilities.sanitize_string res[:error][:message]
            msg += ": #{res[:error][:class]}#{res[:error][:number]}#{error_message}"
          end
        else
          msg += '.'
        end
        raise BindungErrorResponse, msg
      else
        raise UnableToInterpretResponse, 'Server responsed with unknown message type'
      end

      return res
    end

    private

    # Private method, which is not intended to be called from outside.
    #
    # An array of attributes is given. The function then returns a summarized
    # hash with information about the attributes.
    # @param attributes [Hash]
    # @return [Hash]
    def parse_attributes attributes
      res = {}

      attributes.each do |attr|
        type = RFC3489Message::StunMessage::ATTRIBUTE_TYPES[attr[:attribute_type]]
        value = attr[:attribute_value]

        case type
        when :mapped_address
          res.merge! parse_mapped_address(value)
        when :source_address
          res.merge! parse_mapped_address(value, 'source_')
        when :changed_address
          res.merge! parse_mapped_address(value, 'changed_')
        when :error_code
          res[:error] = {} if ! res[:error]
          res[:error][:class] = value[:error_class]
          res[:error][:number] = value[:error_number]
          res[:error][:message] = value[:error_message]
        when :unknown_attributes
          res[:error] = {} if ! res[:error]
          res[:error][:unknown_attributes] = value[:unknown_attributes].join ', '
        end
      end

      return res
    end
  end
end
