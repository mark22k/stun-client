
# This is just a placeholder file to make yard happy

# The StunClient library
module StunClient
  # Contains the version number of STUN Client.
  VERSION = '1.2.1'.freeze
end
